require 'gem2deb/rake/testtask'

TESTS = `grep -l test/unit t/*.rb`.split

Gem2Deb::Rake::TestTask.new do |t|
  t.test_files = TESTS
end
